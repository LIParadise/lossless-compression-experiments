# Lossless Compression Experiments

Random source is `C++` `rand()` function

Using `clang version 12.0.0` on `Archlinux 5.10.44-1-lts`, we have:

|        | `LZMA` | `LZW`  |
| ---    | ---    | ---    |
| slope  | 0.795  | 1.099  |
| R^2    | 0.9998 | 0.9998 |
| offset | 11     | 174    |

We can see that `LZW` perform *better* than `LZMA` for very short, rather high entropy strings; in this experiment, the threshold is around `500` bytes.
