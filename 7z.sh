#!/usr/bin/env sh

output="LZMA_7z_compressed"
input="generated"
if [ "$#" -eq 1 ] && [ -d "$1" ]; then
    input="$1"
    output="$1_LZMA_7z_compressed"
fi
method="LZMA"

mkdir -p "$output"
rm -rf "$output"/*
find "$input" -type f -name "*.txt" -print0 | \
    xargs -0 -P 16 -I{} sh -c 'f="${1##*/}"; 7z a -t7z "$2"/"${f%%.*}.7z" "$1" -m1="$3"' - '{}' "$output" "$method"

# `-P 16`: run up to 16 instances
# `${1##*/}`: delete from front, longest match of regexp "*/"
#             for stripping the directory name
# `${f%%.*}`: delete from back, longest match of regexp ".*"
#             for stripping the filename extension
#
#
# Verbosely, for all files in folder `$input`, use `7z` with `LZMA`
# to compress them individually, and put them in folder `$output`
#
# For example `$input/42.txt` would make `$output/42.7z`
