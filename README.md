# Testing Compression Algorithms on Short Strings

## Usage

``` bash
gcc -O2 -o generate generate.cc
./generate <bits>
./7z.sh
```

`generate.cc` would create `<bits>` files in folder `generated`, i.e. from `generated/1.txt` to
`generated/<bits>.txt`.
Each containing certain number of alphanumerical characters, corresponding to its name.

`7z.sh` would compress all `*.txt` files in folder `generated` and place output
(`.7z` filename extension, `LZMA` compression) in folder `compressed`.

Aforementioned folders would be created automatically if they don't exist.

## Example

``` bash
./generate 69
./7z.sh
```

After which we have `generated/1.txt`, `generated/2.txt`, all the way to `generated/69.txt`,
and `compressed/1.7z`, `compressed/2.7z`, all the way to `compressed/69.7z`.
