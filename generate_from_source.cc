#include <iostream>
#include <string>
#include <ctime>
#include <unistd.h>
#include <fstream>
#include <sstream>

using namespace std;

static const string output = "generated_with_given_source";
static const size_t bits_limit = 2048;

int main(int argc, char** argv){

    system( ("mkdir -p "+output).c_str() );
    system( ("rm -rf "+output+"/*").c_str() );

    if( argc != 3 ){
        cout << "Usage: ./" << argv[0] << " <bits> <file>" << endl;
        cout << endl;
        cout << "Generate <bits> of files, each containing (1..<bits>) of ASCII char, extracted from <file>" << endl;
        cout << "Maximum <bits> is" << bits_limit << "." << endl;
        return 1;
    }

    size_t len = stoul(argv[1]);
    if( len >= bits_limit ){
        len = bits_limit;
    }

    fstream source_file( argv[2], fstream::in );
    if ( !source_file.good() ){
        source_file.close();
        return 1;
    }

    char* source_buf = new char[len+1];
    source_file.read( source_buf, len );
    source_buf[len] = '\0';
    source_file.close();

    for( ; len > 0; --len ){
        stringstream ss;
        ss << len;
        fstream fs( (output + "/" + ss.str() + ".txt"), fstream::out );
        fs << source_buf;
        fs.close();
        source_buf[len] = '\0';
    }
    delete [] source_buf;

    return 0;

}
