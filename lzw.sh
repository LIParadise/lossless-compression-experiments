#!/usr/bin/env sh

output="lzw_ncompress_compressed"
input="generated"
if [ "$#" -eq 1 ] && [ -d "$1" ]; then
    input="$1"
    output="$1_lzw_ncompress_compressed"
fi

mkdir -p "$output"
rm -rf "$output"/*
find "$input" -type f -name "*.txt" -print0 | \
    xargs -0 -P 16 -I{} sh -c 'compress -k -f - "$1"' - '{}'

find "$input" -type f -not -name "*.txt" -print0 | \
    xargs -0 -P 4 -I{} sh -c 'mv "$1" "$2"' - '{}' "$output"
