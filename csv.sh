#!/usr/bin/env sh

find . -type d -name "*compressed" -print0 | \
    xargs -0 -P 4 -I{} sh -c 'echo "Source Alphanumeric Characters, Compressed Size" > "$1"/statistics.csv' - '{}'

find . -type d -name "*compressed" -print0 | \
    xargs -0 -P 4 -I{} sh -c "ls -ASlr \"\$1\" | grep -v statistics | tail -n +2 | awk '{print \$9\",\"\$5}' | sed -r 's/\.7z//' >> \"\$1\"/statistics.csv" - '{}'
