#include <iostream>
#include <string>
#include <ctime>
#include <unistd.h>
#include <fstream>
#include <sstream>

using namespace std;

string gen_rnd_str(const size_t len) {
    string tmp_s;
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
    
    tmp_s.reserve(len);

    for (size_t i = 0; i < len; ++i) 
        tmp_s += alphanum[rand() % (sizeof(alphanum) - 1)];
    
    return tmp_s;
}

static const string output = "generated";
static const size_t bits_limit = 2048;

int main(int argc, char** argv){

    srand( (unsigned) time(NULL) * getpid());
    system( ("mkdir -p "+output).c_str() );
    system( ("rm -rf "+output+"/*").c_str() );

    if( argc != 2 ){
        cout << "Usage: ./" << argv[0] << " <bits>" << endl;
        cout << endl;
        cout << "Generate <bits> of files, each containing (1..<bits>) of ASCII char, respectively" << endl;
        cout << "Maximum <bits> is 100." << endl;
        return 1;
    }

    size_t f = stoul(argv[1]);
    if( f >= bits_limit ){
        f = bits_limit;
    }

    for( ; f > 0; --f ){
        string tmp = gen_rnd_str(f);
        stringstream ss;
        ss << f;
        fstream fs( (output + "/" + ss.str() + ".txt"), fstream::out );
        fs << tmp;
        fs.close();
    }

    return 0;

}
